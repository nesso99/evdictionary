/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdict;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Duc
 */
public class FinalDict {

    private final MyHash data = new MyHash();
    private final StringBuilder wordSuggest = new StringBuilder();

    public FinalDict() throws FileNotFoundException {
        Scanner input = null;
        input = new Scanner(new File("Data/VietEng.txt"));
        while (input.hasNext()) {
            String[] buffer = mySplit(input.nextLine(), '|');
            data.put(buffer[0], buffer[1]);
        }
    }

    public String search(StringBuilder eng) {
        standardize(eng);
        String result = data.get(eng);
        return (result != null ? result : eng.toString());
    }

    public boolean add(StringBuilder eng, String viet) {
        standardize(eng);
        if (data.containsKey(eng)) {
            return false;
        }
        data.putWithOrder(eng.toString(), viet);
        return true;
    }

    public void delete(StringBuilder eng) {
        standardize(eng);
        data.remove(eng);
    }

    public boolean replace(StringBuilder eng, String newViet) {
        standardize(eng);
        if (!data.containsKey(eng)) {
            return false;
        }
        String replaceStr = "<br/><ul><li><font color='#cc0000'><b>"
                + newViet + "</b></font></li></ul>";
        int pos = 0;
        String temp = (String) data.get(eng);
        while (!(temp.charAt(pos) == '<' && temp.charAt(pos + 1) == 'b')) {
            pos++;
        }
        replaceStr = temp.substring(0, pos) + replaceStr;
        data.replace(eng, replaceStr);
        return true;
    }

    private void outToFile(String fileName) throws IOException {
        FileWriter output = new FileWriter(new File(fileName));
        int pos = 1;
        for (String key : data.keySet()) {
            String line = pos + "|" + key + "|" + data.get(new StringBuilder(key)) + "\n";
            output.write(line);
        }
        output.close();
    }

    public StringBuilder wordSuggest(String key) {
        wordSuggest.delete(0, wordSuggest.length());
        ArrayList<String> buffer = data.suggestSet(key);
        if (buffer == null) {
            return wordSuggest;
        }
        for (String word : buffer) {
            wordSuggest.append(word);
            wordSuggest.append("\n");
        }
        return wordSuggest;
    }

    private void standardize(StringBuilder word) {
        while (word.length() > 0 && word.charAt(0) == ' ') {
            word.deleteCharAt(0);
        }
        while (word.length() > 0 && word.charAt(word.length() - 1) == ' ') {
            word.deleteCharAt(word.length() - 1);
        }
    }

    private String[] mySplit(String str, char limit) {
        String[] result = new String[2];
        if (str.isEmpty()) {
            return result;
        }
        int start = 0;
        while (str.charAt(start) != limit) {
            start++;
        }
        int end = start + 1;
        while (str.charAt(end) != limit) {
            end++;
        }
        result[0] = str.substring(start + 1, end).toLowerCase();
        result[1] = str.substring(end + 1);
        return result;
    }
}

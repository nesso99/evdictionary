/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fdict;

import java.util.ArrayList;

/**
 *
 * @author Duc
 */
public class MyHash {

    private final int size = 73;
    private final ArrayList<ArrayList<Pair>> data = new ArrayList<>();
    private final ArrayList<String> keySet = new ArrayList<>();
    private final ArrayList<String> suggestSet = new ArrayList<>();

    public MyHash() {
        for (int i = 0; i < size; i++) {
            data.add(new ArrayList<>());
        }
    }

    public ArrayList<String> keySet() {
        keySet.clear();
        int row = data.size();
        int col;
        for (int first = 0; first < row; first++) {
            col = data.get(first).size();
            for (int second = 0; second < col; second++) {
                keySet.add(data.get(first).get(second).key);
            }
        }
        return keySet;
    }

    public ArrayList<String> suggestSet(String key) {
        suggestSet.clear();
        if (key.isEmpty()) {
            return null;
        }
        int n = key.length();
        int position = (int) (key.charAt(0)) % size;
        ArrayList<Pair> buffer = data.get(position);
        int Bufsize = buffer.size();
        for (int i = 0; i < Bufsize; i++) {
            String temp = buffer.get(i).key;
            if (n <= temp.length() && key.equals(temp.substring(0, n))) {
                suggestSet.add(temp);
            }
            if (suggestSet.size() == 10) {
                break;
            }
        }
        return suggestSet;
    }

    public void replace(StringBuilder key, String replaceStr) {
        if (key.length() == 0) {
            return;
        }
        int position = (int) (key.charAt(0)) % size;
        ArrayList<Pair> buffer = data.get(position);
        for (int i = 0; i < buffer.size(); i++) {
            if (key.toString().equalsIgnoreCase(buffer.get(i).key)) {
                buffer.get(i).value = replaceStr;
            }
        }
    }

    public void remove(StringBuilder key) {
        if (key.length() == 0) {
            return;
        }
        int position = (int) (key.charAt(0)) % size;
        ArrayList<Pair> buffer = data.get(position);
        for (int i = 0; i < buffer.size(); i++) {
            if (key.toString().equalsIgnoreCase(buffer.get(i).key)) {
                buffer.remove(i);
            }
        }
    }

    public boolean containsKey(StringBuilder key) {
        if (key.length() == 0) {
            return false;
        }
        int position = (int) (key.charAt(0)) % size;
        ArrayList<Pair> buffer = data.get(position);
        for (int i = 0; i < buffer.size(); i++) {
            if (key.toString().equalsIgnoreCase(buffer.get(i).key)) {
                return true;
            }
        }
        return false;
    }

    public String get(StringBuilder key) {
        if (key.length() == 0) {
            return null;
        }
        int position = (int) (key.charAt(0)) % size;
        ArrayList<Pair> buffer = data.get(position);
        for (int i = 0; i < buffer.size(); i++) {
            if (key.toString().equalsIgnoreCase(buffer.get(i).key)) {
                return buffer.get(i).value;
            }
        }
        return null;
    }

    public void put(String key, String value) {
        if (key.isEmpty()) {
            return;
        }
        int position = (int) (key.charAt(0)) % size; // 48 la ma ASCII cua '0'
        data.get(position).add(new Pair(key, value));
    }

    public void putWithOrder(String key, String value) {
        if (key.isEmpty()) {
            return;
        }
        int position = ((int) key.charAt(0)) % size; // 48 la ma ASCII cua '0'
        data.get(position).add(new Pair(key, value));
        data.get(position).sort(null);
    }

    private class Pair {

        public String key;
        public String value;

        public Pair(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}
